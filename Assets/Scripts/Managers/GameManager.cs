﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Managers
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Get { get; private set; }

        public static bool IsInitalized { get; private set; }

        private void Awake()
        {
            if (Get == null)
            {
                Get = this;
                IsInitalized = true;
            }
            else
            {
                DestroyImmediate(gameObject);
            }
        }

        private static readonly Dictionary<Type, Component> Services = new Dictionary<Type, Component>();

        public static T GetService<T>() where T : Component
        {
            if (Services.TryGetValue(typeof(T), out var service))
            {
                return (T) service;
            }

            var comp = Get.transform.GetComponentInChildren<T>();
            if (comp != null)
            {
                Services.Add(typeof(T), comp);
                return comp;
            }

            return null;
        }
    }
}