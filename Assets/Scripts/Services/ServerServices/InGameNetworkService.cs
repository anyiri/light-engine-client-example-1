﻿using System.Collections.Generic;
using System.Linq;
using Controllers.GO.Network;
using LightEngineBridge.Model;
using Managers;
using UnityEngine;

namespace Services.ServerServices
{
    public class InGameNetworkService : MonoBehaviour
    {
        public float ServerTime => Time.realtimeSinceStartup - TimeOffset;

        public float TimeOffset { get; set; }

        private readonly Dictionary<ushort, NetworkObjectController> _networkObjects =
            new Dictionary<ushort, NetworkObjectController>();

        private readonly Queue<IInterpolateModel> _delayedEvents = new Queue<IInterpolateModel>();

        public float interpolationDelay = 0.1f;

        public bool GameRunning { get; set; }

        private GameServerConnector _gameServerConnector;

        private void Start()
        {
            _gameServerConnector = GameManager.GetService<GameServerConnector>();
        }

        public void AddDelayedEvent(IInterpolateModel delayedEvent)
        {
            var correctedTimestamp = _gameServerConnector.GetCorrectedTimeStamp(delayedEvent.Timestamp);
            delayedEvent.Timestamp = correctedTimestamp;
            _delayedEvents.Enqueue(delayedEvent);
        }

        private void ProcessEvents()
        {
            while (_delayedEvents.Count > 0 && ServerTime - interpolationDelay >= _delayedEvents.Peek().Timestamp)
            {
                _delayedEvents.Dequeue().Execute();
            }
        }

        private void Update()
        {
            if (!GameRunning) return;
            ProcessEvents();
        }

        public void ClearGame()
        {
            GameRunning = false;
            foreach (var valuePair in _networkObjects)
            {
                Destroy(valuePair.Value.gameObject);
            }
            _networkObjects.Clear();
        }

        public void RegisterNetworkObject(string typeName, ushort id, Vector2 pos)
        {
            if (_networkObjects.ContainsKey(id)) return;
            var prefab = Resources.Load<GameObject>($"NetworkObjects/{typeName}");
            var controller = Instantiate(prefab, pos, Quaternion.identity)
                .GetComponent<NetworkObjectController>();
            controller.Init(id);
            _networkObjects.Add(controller.Id, controller);
        }

        public NetworkObjectController GetNetworkObject(ushort id)
        {
            _networkObjects.TryGetValue(id, out var controller);
            return controller;
        }

        public void DestroyNetworkObject(ushort id)
        {
            var controller = GetNetworkObject(id);
            if (controller != null)
            {
                controller.DestroyMe();
                _networkObjects.Remove(id);
            }
        }

        public void ReSyncObjects(ushort[] gameObjectIds)
        {
            foreach (var keyValue in _networkObjects)
            {
                if (!gameObjectIds.Contains(keyValue.Key))
                {
                    keyValue.Value.DestroyMe();
                }
            }
        }
    }
}