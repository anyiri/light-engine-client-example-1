﻿using System;
using System.Collections.Generic;
using LightEngineBridge.Debug;
using LightEngineBridge.LiteNetLib;
using LightEngineBridge.Model;
using LightEngineBridge.Network;
using LightEngineBridge.Network.Interface;
using LightEngineBridge.Network.Task.Factory;
using LightEngineBridge.Serialization;
using LightEngineBridge.Util;
using Other;
using UnityEngine;
using UnityEngine.Events;

namespace Services.ServerServices
{
    public class GameServerConnector : MonoBehaviour
    {
        public string ipAddress;
        public int port;
        public int updateTime = 15;
        public string connectionKey;

        private readonly GUIStyle _style = new GUIStyle();

        public bool connectAutomatically = true;
        public bool simulateLatency = false;
        public bool simulatePacketloss = false;
        public bool showStatistics = false;
        public int statisticsFontSize = 50;

        public UnityEvent onConnected;

        private EventBasedNetListener _listener;
        private NetManager _client;

        public int Latency { get; protected set; }

        private NetPeer _serverPeer;

        private bool _shouldReconnect = true;

        private IRequestHandler _requestHandler;

        public bool IsConnected =>
            _client?.FirstPeer != null && _client.FirstPeer.ConnectionState == ConnectionState.Connected;

        public bool IsLoggedIn { get; set; }
        public bool IsStaticDataUpToDate { get; set; }

        public TaskSenderFactory SenderFactory { get; set; }

        private readonly Dictionary<byte, Action> _onCompletedActions = new Dictionary<byte, Action>();

        private void Init()
        {
            _shouldReconnect = connectAutomatically;
            _client?.Stop();

            _listener = new EventBasedNetListener();
            _client = new NetManager(_listener);
            if (simulateLatency)
            {
                _client.SimulateLatency = true;
                _client.SimulationMinLatency = 500;
                _client.SimulationMaxLatency = 800;
            }

            if (simulatePacketloss)
            {
                _client.SimulatePacketLoss = true;
                _client.SimulationPacketLossChance = 20;
            }

            IsLoggedIn = false;
            IsStaticDataUpToDate = false;
            LightDebug.Logger = new UnityLogger();
            SenderFactory = new TaskSenderFactory();
            _requestHandler = new NetworkTaskRequestHandler();
        }

        public void Connect()
        {
            _client.Start();
            _client.UpdateTime = updateTime;
            _client.Connect(ipAddress, port, connectionKey);
            _client.ReconnectDelay = 1000;
            _client.MaxConnectAttempts = 9999;

            _listener.NetworkErrorEvent += (point, code) => { Debug.LogError("Error occured, ErrorCode: " + code); };

            _listener.PeerConnectedEvent += (peer) =>
            {
                _serverPeer = peer;
                _shouldReconnect = true;
                onConnected?.Invoke();
            };

            _listener.NetworkReceiveEvent += (peer, reader, deliveryMethod) =>
            {
                try
                {
                    var commandByte = reader.GetByte();
                    _requestHandler.HandleRequest(commandByte, reader, peer, null);
                    if (_onCompletedActions.TryGetValue(commandByte, out var onExecutedCallback))
                    {
                        onExecutedCallback();
                    }
                }
                catch (Exception e)
                {
                    Debug.LogError(e.Message);
                    Debug.LogError(e.StackTrace);
                    if (e.InnerException != null)
                    {
                        Debug.LogError(e.InnerException.Message);
                        Debug.LogError(e.InnerException.StackTrace);
                    }
                }
            };

            _listener.NetworkLatencyUpdateEvent += (peer, latency) => { this.Latency = latency; };
        }

        public float GetTimeOffset(float timeStamp)
        {
            return Time.realtimeSinceStartup - timeStamp + Latency / 1000f;
        }

        public float GetCorrectedTimeStamp(float timeStamp)
        {
            return timeStamp + GetRoundTripTime();
        }

        public float GetRoundTripTime()
        {
            return Latency / 2f / 1000f;
        }

        #region Wrappers

        public void RequestEmpty<TNetworkCommand>(TNetworkCommand command, Action onExecutedCallback = null)
            where TNetworkCommand : struct, IConvertible
        {
            if (_serverPeer != null)
            {
                if (onExecutedCallback != null)
                {
                    var commandByte = command.Cast<byte>();
                    if (_onCompletedActions.ContainsKey(commandByte))
                    {
                        _onCompletedActions[commandByte] = onExecutedCallback;
                    }
                    else
                    {
                        _onCompletedActions.Add(commandByte, onExecutedCallback);
                    }
                }

                SenderFactory.CreateSender(_serverPeer)
                    .Send(command.Cast<byte>(), DeliveryMethod.ReliableOrdered);
            }
            else
            {
                Debug.LogWarning("Cannot request because you are not connected");
            }
        }

        public void Request<TSendModel>(TSendModel sendModel)
            where TSendModel : BridgeModel
        {
            if (_serverPeer != null)
            {
                SenderFactory.CreateSender<TSendModel>(_serverPeer)
                    .Send(DeliveryMethod.ReliableOrdered, sendModel);
            }
            else
            {
                Debug.LogWarning("Cannot request because you are not connected");
            }
        }

        public void Request<TNetworkCommand, TSendModel>(TNetworkCommand command, TSendModel sendModel,
            Action onExecutedCallback = null)
            where TSendModel : BridgeModel
            where TNetworkCommand : struct, IConvertible
        {
            if (_serverPeer != null)
            {
                if (onExecutedCallback != null)
                {
                    var commandByte = command.Cast<byte>();
                    if (_onCompletedActions.ContainsKey(commandByte))
                    {
                        _onCompletedActions[commandByte] = onExecutedCallback;
                    }
                    else
                    {
                        _onCompletedActions.Add(commandByte, onExecutedCallback);
                    }
                }

                SenderFactory.CreateSender<TSendModel>(_serverPeer)
                    .Send(command, DeliveryMethod.ReliableOrdered, sendModel);
            }
            else
            {
                Debug.LogWarning("Cannot request because you are not connected");
            }
        }

        public void RequestSequenced<TSendModel>(TSendModel sendModel)
            where TSendModel : BridgeModel
        {
            if (_serverPeer != null)
            {
                SenderFactory.CreateSender<TSendModel>(_serverPeer)
                    .Send(DeliveryMethod.Sequenced, sendModel);
            }
            else
            {
                Debug.LogWarning("Cannot request because you are not connected");
            }
        }

        public void RequestSequenced<TNetworkCommand, TSendModel>(TNetworkCommand command, TSendModel sendModel)
            where TSendModel : BridgeModel
            where TNetworkCommand : struct, IConvertible
        {
            if (_serverPeer != null)
            {
                SenderFactory.CreateSender<TSendModel>(_serverPeer)
                    .Send(command, DeliveryMethod.Sequenced, sendModel);
            }
            else
            {
                Debug.LogWarning("Cannot request because you are not connected");
            }
        }

        #endregion

        #region Monos

        private void Start()
        {
            _style.normal.textColor = Color.green;
            _style.fontStyle = FontStyle.Bold;
            _style.fontSize = 50;
            Init();
            OnValidate();
        }

        private void Update()
        {
            if (_shouldReconnect && !IsConnected)
            {
                Init();
                Connect();
                _shouldReconnect = false;
            }

            _client.PollEvents();
        }

        private void OnApplicationQuit()
        {
            _client.Stop();
        }

        private void OnGUI()
        {
            if (showStatistics && IsConnected)
            {
                var stats = _client.Statistics;
                int increment = statisticsFontSize;
                int offset = Screen.height - increment * 4;
                GUI.Label(new Rect(10, offset, 100, 20), "Latency: " + Latency + " ms", _style);
                offset += increment;
                GUI.Label(new Rect(10, offset, 100, 20), "Bytes Received: " + stats.BytesReceived, _style);
                offset += increment;
                GUI.Label(new Rect(10, offset, 100, 20), "Bytes Sent: " + stats.BytesSent, _style);
                offset += increment;
                GUI.Label(new Rect(10, offset, 100, 20), "Packet loss: " + stats.PacketLossPercent + "%", _style);
            }
            else if (showStatistics)
            {
                GUI.Label(new Rect(10, 10, 100, 20), "Not Connected", _style);
            }
        }

        private void OnValidate()
        {
            _style.fontSize = statisticsFontSize;
        }

        #endregion
    }
}