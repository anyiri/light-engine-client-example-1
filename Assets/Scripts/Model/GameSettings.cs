﻿using UnityEngine;

namespace Model
{
    [System.Serializable]
    public class GameSettings 
    {
        public bool HighFrameRateEnabled { get; set; }
        public bool ShowFrameRate { get; set; }
    }
}
