﻿using LightEngineBridge.Attribute;
using LightEngineBridge.Network.Task.Base;
using LightVolleyBallBridge.Enum;
using LightVolleyBallBridge.Model;

namespace Network.Task.Communication
{
    [NetworkTask(NetworkCommand.TestMessage)]
    public class TestMessageTask : ReceiverNetworkTask<StringModel>
    {
        public override void Execute()
        {
            UnityEngine.Debug.Log($"Server said: {Data.Value}");
        }
    }
}