﻿using LightEngineBridge.Attribute;
using LightEngineBridge.Network.Task.Base;
using LightVolleyBallBridge.Enum;
using LightVolleyBallBridge.Model;
using Managers;
using Services.ServerServices;
using UnityEngine;

namespace Network.Task.Communication
{
    [NetworkTask(NetworkCommand.GameObjectSpawn)]
    public class GameObjectSpawnTask : ReceiverNetworkTask<GameObjectSpawnModel>
    {
        private readonly InGameNetworkService _networkService;

        public GameObjectSpawnTask()
        {
            _networkService = GameManager.GetService<InGameNetworkService>();
        }

        public override void Execute()
        {
            _networkService.RegisterNetworkObject(Data.NetworkObjectType.ToString(), Data.GameObjectId,
                new Vector2(Data.PosX, Data.PosY));
        }
    }
}