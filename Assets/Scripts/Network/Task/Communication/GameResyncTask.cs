﻿using LightEngineBridge.Attribute;
using LightEngineBridge.Network.Task.Base;
using LightVolleyBallBridge.Enum;
using LightVolleyBallBridge.Model;
using Managers;
using Services.ServerServices;

namespace Network.Task.Communication
{
    [NetworkTask(NetworkCommand.GameResync)]
    public class GameResyncTask : ReceiverNetworkTask<GameResyncModel>
    {
        public override void Execute()
        {
            var netService = GameManager.GetService<InGameNetworkService>();
            netService.ReSyncObjects(Data.GameObjectIds);
        }
    }
}
