﻿using Controllers.GO.Network;
using LightEngineBridge.Attribute;
using LightEngineBridge.Network.Task.Base;
using LightEngineBridge.Util;
using LightVolleyBallBridge.Enum;
using LightVolleyBallBridge.Model;
using Managers;
using Services.ServerServices;
using UnityEngine;

namespace Network.Task.Communication
{
    [NetworkTask(NetworkCommand.PositionSync)]
    public class PositionSyncTask : ReceiverNetworkTask<PositionSyncModel>
    {
        private readonly InGameNetworkService _networkService;

        public PositionSyncTask()
        {
            _networkService = GameManager.GetService<InGameNetworkService>();
        }

        public override void Execute()
        {
            var controller = _networkService.GetNetworkObject(Data.GameObjectId);
            if (controller != null)
                controller.GetComponent<TransformSyncController>()
                    .AddPos(new Vector3(Data.PosX.ToFloat(), Data.PosY.ToFloat()), Data.Timestamp);
        }
    }
}