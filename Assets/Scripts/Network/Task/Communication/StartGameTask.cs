﻿using LightEngineBridge.Attribute;
using LightEngineBridge.Network.Task.Base;
using LightVolleyBallBridge.Enum;
using LightVolleyBallBridge.Model;
using Managers;
using Services.ServerServices;

namespace Network.Task.Communication
{
    [NetworkTask(NetworkCommand.StartGame)]
    public class StartGameTask : ReceiverNetworkTask<StartGameModel>
    {
        public override void Execute()
        {
            var networkService = GameManager.GetService<InGameNetworkService>();
            networkService.GameRunning = true;
            networkService.TimeOffset =
                GameManager.GetService<GameServerConnector>().GetTimeOffset(Data.Timestamp);
            UnityEngine.Debug.Log("Game started");
        }
    }
}
