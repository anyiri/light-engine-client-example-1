﻿using LightEngineBridge.Attribute;
using LightEngineBridge.Network.Task.Base;
using LightVolleyBallBridge.Enum;
using LightVolleyBallBridge.Model;
using Managers;
using Services.ServerServices;

namespace Network.Task.Communication
{
    [NetworkTask(NetworkCommand.GameObjectDestroy)]
    public class GameObjectDestroyTask : ReceiverNetworkTask<GameObjectDestroyModel>
    {
        private readonly InGameNetworkService _networkService;

        public GameObjectDestroyTask()
        {
            _networkService = GameManager.GetService<InGameNetworkService>();
        }

        public override void Execute()
        {
            _networkService.DestroyNetworkObject(Data.GameObjectId);
        }
    }
}