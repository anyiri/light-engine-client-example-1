﻿using Controllers.GO.Network;
using LightEngineBridge.Attribute;
using LightEngineBridge.Network.Task.Base;
using LightEngineBridge.Util;
using LightVolleyBallBridge.Enum;
using LightVolleyBallBridge.Model;
using Managers;
using Services.ServerServices;
using UnityEngine;

namespace Network.Task.Communication
{
    [NetworkTask(NetworkCommand.TransformSync)]
    public class TransformSyncTask : ReceiverNetworkTask<TransformSyncModel>
    {
        private readonly InGameNetworkService _networkService;

        public TransformSyncTask()
        {
            _networkService = GameManager.GetService<InGameNetworkService>();
        }

        public override void Execute()
        {
            var controller = _networkService.GetNetworkObject(Data.GameObjectId);
            if (controller != null)
                controller.GetComponent<TransformSyncController>()
                    .AddPosRot(new Vector3(Data.PosX, Data.PosY), Data.Rotation, Data.Timestamp);
        }
    }
}