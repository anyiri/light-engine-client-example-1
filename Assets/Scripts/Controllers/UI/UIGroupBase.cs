﻿using UnityEngine;

namespace Controllers.UI
{
    public class UIGroupBase : MonoBehaviour
    {
        public virtual void LoadDataSource(RouteData routeData = null)
        {
        }

        public void CallLoadDataSource()
        {
            LoadDataSource(null);
        }

        public virtual void ClearContent()
        {

        }
    }
}