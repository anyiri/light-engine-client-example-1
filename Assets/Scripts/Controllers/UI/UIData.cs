﻿namespace Controllers.UI
{
    public struct UIData
    {
        public string DataName { get; set; }
        public object Data { get; set; }
    }
}
