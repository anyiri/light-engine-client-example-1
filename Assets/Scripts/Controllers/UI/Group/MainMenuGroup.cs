﻿using LightVolleyBallBridge.Enum;
using LightVolleyBallBridge.Model;
using Managers;
using Services.ServerServices;
using TMPro;
using UnityEngine;

namespace Controllers.UI.Group
{
    public class MainMenuGroup : UIGroupBase
    {
        [SerializeField] private TMP_InputField _testInputField;

        private GameServerConnector _gameServerConnector;

        private void Start()
        {
            _gameServerConnector = GameManager.GetService<GameServerConnector>();
        }

        public void SendTestMessage()
        {
            var message = _testInputField.text;
            _gameServerConnector.Request(NetworkCommand.TestMessage, new StringModel {Value = message});
        }

        public void StartGame()
        {
            _gameServerConnector.RequestEmpty(NetworkCommand.StartGame);
        }
    }
}