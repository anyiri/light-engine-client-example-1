﻿using System.Collections.Generic;
using System.Linq;

namespace Controllers.UI
{
    public class UIDataSource 
    {
        public List<UIData> Data { get; set; }

        public UIDataSource(params UIData[] uiData)
        {
            Data = uiData.ToList();
        }
    }
}
