﻿using System.Collections.Generic;

namespace Controllers.UI
{
    public class RouteData 
    {
        private readonly Dictionary<string, object> _data = new Dictionary<string, object>();

        public RouteData(params UIData[] data)
        {
            foreach (var uiData in data)
            {
                _data.Add(uiData.DataName, uiData.Data);
            }

        }

        public bool TryGetValue<TData>(string dataName, out TData outValue)
        {
            if (_data.TryGetValue(dataName, out var value))
            {
                outValue = (TData)value;
                return true;
            }
            outValue = default;
            return false;
        }
    }
}
