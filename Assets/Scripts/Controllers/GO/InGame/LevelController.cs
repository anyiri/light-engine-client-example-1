﻿using LightVolleyBallBridge.Enum;
using LightVolleyBallBridge.Model;
using Managers;
using Services.ServerServices;
using UnityEngine;

namespace Controllers.GO.InGame
{
    public class LevelController : MonoBehaviour
    {
        public void MakeExplosion(Vector3 position)
        {
            if (!GameManager.GetService<InGameNetworkService>().GameRunning) return;

            GameManager.GetService<GameServerConnector>().Request(NetworkCommand.MakeExplosion,
                new MakeExplosionModel {PosX = position.x, PosY = position.y});
        }
    }
}