﻿using Managers;
using Services.ServerServices;
using UnityEngine;
using UnityEngine.Events;

namespace Controllers.GO.Network
{
    public class NetworkObjectController : MonoBehaviour
    {
        public UnityEvent onInit = new UnityEvent();

        public ushort Id { get; private set; }

        private InGameNetworkService _netService;
        private InGameNetworkService NetService =>
            _netService ? _netService : _netService = GameManager.GetService<InGameNetworkService>();

        public void Init(ushort id)
        {
            this.Id = id;
            onInit?.Invoke();
        }

        public virtual void DestroyMe()
        {
            Destroy(gameObject);
        }
    }
}
