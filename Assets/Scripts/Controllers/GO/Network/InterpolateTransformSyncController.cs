﻿using System.Collections.Generic;
using Managers;
using Services.ServerServices;
using UnityEngine;

namespace Controllers.GO.Network
{
    public class InterpolateTransformSyncController : TransformSyncController
    {
        private class Snapshot
        {
            public Vector3 Pos { get; set; }
            public float Rot { get; set; }
            public float TimeStamp { get; set; }
        }

        private class State
        {
            public Snapshot Prev { get; set; }
            public Snapshot Next { get; set; }
        }

        private readonly LinkedList<Snapshot> _snapshots = new LinkedList<Snapshot>();
        private InGameNetworkService _networkService;

        public int snapshotBufferSize = 100;

        private State _currentState;

        public bool interpolationEnabled = true;

        private Vector3 _lastCorrectPos;
        private float _lastCorrectRot;
        private float _currentRot;

        public float lerpToCorrectSpeed = 15.5f;

        public override void Init()
        {
            _snapshots.Clear();
            _lastCorrectPos = transform.position;
            _lastCorrectRot = transform.eulerAngles.z;
            _currentRot = _lastCorrectRot;
            _currentState = null;
        }

        private void Start()
        {
            _networkService = GameManager.GetService<InGameNetworkService>();
        }

        public override void AddPosRot(Vector3 pos, float rot, float timeStamp)
        {
            if (interpolationEnabled)
            {
                _snapshots.AddFirst(new Snapshot
                {
                    Pos = pos,
                    Rot = rot,
                    TimeStamp = timeStamp
                });
                if (_snapshots.Count >= snapshotBufferSize)
                {
                    _snapshots.RemoveLast();
                }
            }
            else
            {
                var trans = transform;
                trans.position = pos;
                trans.eulerAngles = new Vector3(0, 0, rot);
                _currentRot = rot;
            }
        }

        public override void AddPos(Vector3 pos, float timeStamp)
        {
            if (interpolationEnabled)
            {
                _snapshots.AddFirst(new Snapshot
                {
                    Pos = pos,
                    TimeStamp = timeStamp
                });
                if (_snapshots.Count >= snapshotBufferSize)
                {
                    _snapshots.RemoveLast();
                }
            }
            else
            {
                transform.position = pos;
            }
        }

        private State GetCurrentState(float checkTimeStamp)
        {
            Snapshot prev = null;
            foreach (var snapshot in _snapshots)
            {
                var current = snapshot;
                if (prev != null && current.TimeStamp < checkTimeStamp && checkTimeStamp <= prev.TimeStamp)
                {
                    return new State
                    {
                        Prev = current,
                        Next = prev
                    };
                }

                prev = current;
            }

            if (_snapshots.Count > 0 && checkTimeStamp > _snapshots.First.Value.TimeStamp)
            {
                transform.position =
                    Vector3.Lerp(transform.position, _snapshots.First.Value.Pos, Time.deltaTime * lerpToCorrectSpeed);
                _currentRot = Mathf.Lerp(_currentRot, _snapshots.First.Value.Rot, Time.deltaTime * lerpToCorrectSpeed);
                transform.eulerAngles = new Vector3(0, 0, _currentRot);
            }

            return null;
        }

        public override void Process()
        {
            float currentTimeStamp = _networkService.ServerTime - _networkService.interpolationDelay;

            if (_currentState != null && currentTimeStamp <= _currentState.Next.TimeStamp)
            {
                var length = _currentState.Next.TimeStamp - _currentState.Prev.TimeStamp;
                var fraction = (currentTimeStamp - _currentState.Prev.TimeStamp) / length;
                _lastCorrectPos = Vector3.Lerp(_currentState.Prev.Pos, _currentState.Next.Pos, fraction);
                _lastCorrectRot = Mathf.Lerp(_currentState.Prev.Rot, _currentState.Next.Rot, fraction);
            }
            else
            {
                _currentState = GetCurrentState(currentTimeStamp);
            }

            transform.position =
                Vector3.Lerp(transform.position, _lastCorrectPos, Time.deltaTime * lerpToCorrectSpeed);
            _currentRot = Mathf.Lerp(_currentRot, _lastCorrectRot, Time.deltaTime * lerpToCorrectSpeed);
            transform.eulerAngles = new Vector3(0, 0, _currentRot);
        }

        private void Update()
        {
            if (interpolationEnabled) Process();
        }
    }
}