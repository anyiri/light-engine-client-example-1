﻿using UnityEngine;

namespace Controllers.GO.Network
{
    public class NetworkAnimatorController : MonoBehaviour
    {
        public Animator anim;

        public void SetAnimBool(string paramName, bool value)
        {
            if (anim != null) anim.SetBool(paramName, value);
        }

        public void SetAnimTrigger(string paramName)
        {
            if (anim != null) anim.SetTrigger(paramName);
        }
    }
}
