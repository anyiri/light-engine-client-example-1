﻿using UnityEngine;

namespace Controllers.GO.Network
{
    public class LerpSyncController : TransformSyncController
    {
        private Vector3 _latestCorrectPos;
        private Vector3 _onUpdatePos;
        private float _fraction;

        public float fractionMultiplier = 11f;

        public override void Init()
        {
            this._latestCorrectPos = transform.position;
            this._onUpdatePos = transform.position;
        }

        public override void AddPos(Vector3 pos, float timestamp)
        {
            if (Vector2.Distance(pos, transform.position) > 10f)
            {
                transform.position = pos;
            }

            _fraction = 0;
            this._latestCorrectPos = pos;
            this._onUpdatePos = transform.position;
        }

        public override void AddPosRot(Vector3 pos, float rot, float timeStamp)
        {
        }

        public override void Process()
        {
            this._fraction = this._fraction + Time.deltaTime * fractionMultiplier;
            transform.position = Vector3.Lerp(this._onUpdatePos, this._latestCorrectPos, this._fraction);
        }

        public void SnapToLastCorrect()
        {
            transform.position = _latestCorrectPos;
        }

        private void Update()
        {
            Process();
        }
    }
}
