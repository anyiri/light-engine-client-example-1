﻿using UnityEngine;

namespace Controllers.GO.Network
{
    public abstract class TransformSyncController : MonoBehaviour
    {
        public abstract void Init();
        public abstract void AddPos(Vector3 pos, float timeStamp);
        public abstract void AddPosRot(Vector3 pos, float rot, float timeStamp);
        public abstract void Process();
    }
}