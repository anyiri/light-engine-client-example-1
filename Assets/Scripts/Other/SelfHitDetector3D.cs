﻿using UnityEngine;
using UnityEngine.Events;

namespace Other
{
    public class SelfHitDetector3D : MonoBehaviour
    {
        [System.Serializable]
        public class Vector3Event : UnityEvent<Vector3>
        {
        }

        public LayerMask layerMask;

        [SerializeField] private Camera _camera;
        private RaycastHit _rayHit;

        public Vector3Event onHitEvent;
        public UnityEvent onReleaseEvent;

        private bool _dragging;

        private void Update()
        {
            if (Input.GetMouseButtonDown(0) || Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
            {
                var ray = _camera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out _rayHit, 999, layerMask))
                {
                    var comp = _rayHit.transform.GetComponent<SelfHitDetector3D>();
                    if (comp != null && comp.GetInstanceID() == GetInstanceID())
                    {
                        _dragging = true;
                        onHitEvent?.Invoke(_rayHit.point);
                    }
                }
            }
            else if (Input.GetMouseButtonUp(0) || Input.touchCount > 0 &&
                     (Input.touches[0].phase == TouchPhase.Canceled ||
                      Input.touches[0].phase == TouchPhase.Ended))
            {
                if (_dragging)
                {
                    _dragging = false;
                    onReleaseEvent?.Invoke();
                }
            }
        }
    }
}