﻿using System;
using LightEngineBridge.Debug;

namespace Other
{
    public class UnityLogger : ILightLogger
    {
        public void Info(string message)
        {
            UnityEngine.Debug.Log(message);
        }

        public void Info(Exception ex)
        {
            UnityEngine.Debug.Log(ex.ToString());
        }

        public void Warn(string message)
        {
            UnityEngine.Debug.LogWarning(message);
        }

        public void Warn(Exception ex)
        {
            UnityEngine.Debug.LogWarning(ex.ToString());
        }

        public void Error(string message)
        {
            UnityEngine.Debug.LogError(message);
        }

        public void Error(Exception ex)
        {
            UnityEngine.Debug.LogError(ex.ToString());
        }
    }
}
